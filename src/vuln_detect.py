#!/usr/bin/python3

"""
    File name: vuln_detect.py
    Author: Luka Srdarev
    Python Version: 3+
"""

import json
import sys
import os
import argparse
import datetime
import time
import warnings
import subprocess
import requests
import urllib3
from bs4 import BeautifulSoup
import re
from packaging import version
from Wappalyzer import Wappalyzer, WebPage
from pymongo import MongoClient
import wget

args = None


def parse_args():
    parser = argparse.ArgumentParser(description="Detect vulnerabilities in websites source code. ")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-f", "--file", help="Specify the path to javascript file.")
    group.add_argument("-u", "--url", help="Specify the url of the website.")
    group.add_argument("-d", "--dir", help="Specify the path to directory with javascript files.")
    parser.add_argument("-l", "--log", help="Save the output of script in log.txt", action='store_true')
    parser.add_argument("-w", "--warn", help="Show additional warnings.", action='store_true')
    parser.add_argument("-r", "--db", help="Update database used for js vulnerabilites.", action='store_true')
    global args
    args = parser.parse_args()

    if len(sys.argv) < 2:
        parser.print_usage()
        sys.exit(1)

    if args.file:
        scan_source_code(args.file)
    elif args.url:
        get_page_source_from_url(args.url)
    elif args.dir:
        scan_all_files_in_dir(args.dir)


def scan_source_code(filepath, scraped=None, url=None):
    # check readable -> extract libs and cms -> find vulns and dangrous funs-> print/write to log file
    global args

    if scraped is not None:  # site content given as string, not as path to file
        soup = BeautifulSoup(scraped, 'html.parser')
        page_contents = scraped
    else:  # path to javascript/html file is provided
        try:
            with open(filepath, encoding="utf8", errors='ignore') as fp:
                page_contents = fp.read()
                soup = BeautifulSoup(page_contents, 'html.parser')
        except IOError:
            print("Error: could not read file {}".format(filepath))
            sys.exit(1)

    # print("-" * os.get_terminal_size()[0])
    print("-" * 50)
    if filepath is not None:
        print("Scan results for file: {}\n".format(filepath))
        if args.log: log_data(["Scan results for file: {}\n".format(filepath)])
    else:
        print("Scan results for site: {}\n".format(url))
        if args.log: log_data(["Scan results for site: {}\n".format(url)])

    # -------------------------------------------------------------------------------------
    if args.warn:
        display_warnings(page_contents)  # Scan for dangerous javascript functions

    find_vulnerable_libraries(soup)  # Scan for usage of vulnerable or outdated javascript libraries

    if url is not None:
        find_vulnerable_cms(url)  # Scan for usage of vulnerable or outdated CMS versions
    # -------------------------------------------------------------------------------------


def find_vulnerable_libraries(soup):
    libs = []

    script_tags = soup.findAll('script', {"src": True})
    for script_tag in script_tags:
        lib_name = check_vulnerable_lib(script_tag.get('src'))
        if lib_name is not None:
            libs.append(lib_name)
        # check_vulenrable(library, lib_version)
    if not libs:
        print(" [+]  No vulnerable libraries found.")
        if args.log: log_data([" [+]  No vulnerable libraries found."])


def check_vulnerable_lib(script_tag_src):
    global args
    lib_name_ret = None

    if args.db:
        #  provjeriti
        if os.path.exists("jsrepository.json"):
            os.remove("jsrepository.json")
        db_url = "https://raw.githubusercontent.com/RetireJS/retire.js/master/repository/jsrepository.json"
        db_filename = wget.download(db_url)
        print(" [+] Database updated")

    json_file = open("jsrepository.json", "r")
    vuln_libs_json = json.load(json_file)

    for library in vuln_libs_json:

        # scan for libraries imported localy on website without CDN
        try:
            filename_pattern_list = vuln_libs_json[library]["extractors"]["filename"]
        except KeyError:
            continue

        for lib_filename_regex in filename_pattern_list:
            # lib_filename_regex = re.sub(r'[^\x00-\x7f]', r'', lib_filename_regex).replace(u"version", "[0-9][0-9.a-z_\\\\-]+")
            lib_filename_regex = lib_filename_regex.replace(u"§§version§§", "[0-9][0-9.a-z_\\\\-]+")
            match = re.search(lib_filename_regex, script_tag_src)
            if match:
                lib_name_ret = library
                lib_version = match.group(1)
                lib_version = re.search("([0-9.]+[0-9])", lib_version).group(0)
                print(" >>  library detected:", lib_name_ret)
                print(" >>  version:", lib_version)

                for vuln in vuln_libs_json[library]["vulnerabilities"]:
                    if check_vulnerable_lib_version(lib_version, vuln):
                        formatted_print_vuln_lib(vuln, lib_name_ret, lib_version)

            # scan for libraries included using CDN
            try:
                uri_pattern_list = vuln_libs_json[library]["extractors"]["uri"]
            except KeyError:
                continue

            for lib_uri_regex in uri_pattern_list:
                lib_uri_regex = lib_uri_regex.replace(u"§§version§§", "[0-9][0-9.a-z_\\\\-]+")
                match = re.search(lib_uri_regex, script_tag_src)
                if match:
                    lib_name_ret = library
                    lib_version = match.group(1)
                    lib_version = re.search("([0-9.]+[0-9])", lib_version).group(0)
                    print(" >>  library detected:", lib_name_ret)
                    print(" >>  version:", lib_version)

                    for vuln in vuln_libs_json[library]["vulnerabilities"]:
                        if check_vulnerable_lib_version(lib_version, vuln):
                            formatted_print_vuln_lib(vuln, lib_name_ret, lib_version)

    return lib_name_ret


def check_vulnerable_lib_version(lib_version, vuln):
    global args
    is_vulnerable_flag = False
    vulnerable_below, vulnerable_at_or_above = None, None

    # "below" argument is provided for every vulnerability, atOrAbove for some
    if "below" in vuln.keys():
        vulnerable_below = vuln["below"]

    if "atOrAbove" in vuln.keys():
        vulnerable_at_or_above = vuln["below"]

    # vulnerabily given as range of versions below_ver > ver >= at_or_above
    if vulnerable_at_or_above is not None:
        if version.parse(vulnerable_below) > version.parse(lib_version) >= version.parse(vulnerable_at_or_above):
            return True

    # all versions below ceratain version are vulnerable
    elif version.parse(lib_version) < version.parse(vulnerable_below):
        return True

    return False


def formatted_print_vuln_lib(vuln, lib_name, lib_version):
    global args
    output_lines = []

    output_lines.append("=====================================================")
    output_lines.append(" [+] Vulnerable library detected: " + lib_name)
    output_lines.append(" [+] Vulnerable version: " + lib_version)
    output_lines.append(" [+] summary: " + vuln["identifiers"]["summary"])

    # not all entries have a related CVE
    try:
        output_lines.append(" [+] CVE:" + "; ".join(vuln["identifiers"]["CVE"]))
    except KeyError:
        pass

    output_lines.append(" [+] severity: " + vuln["severity"])
    output_lines.append(" [+] info: " + " ".join(vuln["info"]))

    for line in output_lines:
        print(line)
    print()

    if args.log:
        log_data(output_lines)


def find_vulnerable_cms(url):
    warnings.simplefilter("ignore")
    wappalyzer = Wappalyzer.latest()
    try:
        webpage = WebPage.new_from_url(url)
    except Exception:
        print("Error while detecting cms.")
        sys.exit(1)
    print("Retrieved website from: ", url)
    site_components = wappalyzer.analyze_with_versions_and_categories(webpage)

    CMS_name, CMS_version = None, None

    output_lines = []
    output_lines.append("=====================================================")
    for item in site_components:
        if "CMS" in site_components[item]["categories"]:
            CMS_name = item
            output_lines.append(CMS_name)
            CMS_version = site_components[item]["versions"]
            if not CMS_version:
                CMS_version = ["unknown"]
            CMS_version = CMS_version[0]
            output_lines.append(CMS_version)
            break

    if CMS_name is not None:
        print("\n" * 4)
        print(" [+] CMS detected: " + CMS_name)
        print(" [+] version: " + "".join(CMS_version))

        if CMS_version != "unknown":
            print(" [+] Searching for vulnerabilties and matching exploits...")
            print("\n" * 2)
            # os.system("searchsploit " + CMS_name + " " + CMS_version[0])
            subprocess_var = subprocess.Popen("searchsploit " + CMS_name + " " + CMS_version, shell=True, stdout=subprocess.PIPE)
            subprocess_return = subprocess_var.stdout.read()
            for line in subprocess_return.decode("utf-8").split("\n"):
                print(line)


def log_data(lines):
    # log_time_date = datetime.datetime.now().isoformat(' ', 'hours')
    # file_name = log_time_date.replace(":", "-").replace(" ", "--") + "h" ".txt"
    log_time_date = datetime.datetime.now()
    file_name = str(log_time_date.year) + "-" \
                + str(log_time_date.day) + "-" \
                + str(log_time_date.month) + "_" \
                + str(log_time_date.hour) + "hrs" \
                + str(log_time_date.minute) + "mins" \
                + ".txt"

    dir_path = os.getcwd()
    if not os.path.exists("logs"):
        os.makedirs("logs")

    file_path = os.path.join(dir_path, "logs", file_name)
    file = open(file_path, "a")
    for line in lines:
        file.write(line + "\n")
    file.write("\n")
    file.close()


def get_page_source_from_url(url):
    # try to get source code from database
    # if there is no such entry in database create a get request to get page contents
    

    try:
        connection_str_file = open("database_login_data.txt", "r")
        connetion_string = connection_str_file.read()
    except FileNotFoundError:
        print("Login credentials must be supplied in database_login_data.txt")
        print("No such file found")
        sys.exit(1)  

    # client = MongoClient("mongodb://rouser:MiLaBiLaFiLa123@127.0.0.1:27017/websecradar?authSource=websecradar")
    client = MongoClient(connetion_string)
    mydb = client["websecradar"]
    webpages = mydb["crawled_data_pages_v0"]
    urls = mydb["crawled_data_urls_v0"]

    url_with_protocol = url
    url_entry = urls.find_one({"url": url})  # None if theres no such url in db
    if url_entry is None:
        url_with_protocol = "https://" + url
        url_entry = urls.find_one({"url": url_with_protocol})
    if url_entry is None:
        url_with_protocol = "http://" + url
        url_entry = urls.find_one({"url": url_with_protocol})

    if url_entry is not None:
        print("\nRetrieving webpage contents from database")
        print("Found entry for url: ", url_with_protocol)
        number_of_checks = len(url_entry["checks"])
        last_hash = url_entry["checks"][number_of_checks - 1]["hash"]
        website_entry = webpages.find_one({"hash": last_hash})
        if website_entry is not None:
            website_contents = website_entry["page"]
            scan_source_code(None, website_contents, url_with_protocol)
        else:
            print("No website contents available in database.")

    else:
        # failed to obtain website contents from database
        print("\nNo such entry in database, retrieving website contents via http get...")
        urllib3.disable_warnings()
        r = None
        if url.startswith("http"):
            try:
                r = requests.get(url, verify=False, timeout=5)
            except Exception:
                print("Error while trying to create http GET request")
        else:
            try:
                r = requests.get("https://" + url, verify=False, timeout=5)
            except requests.exceptions.RequestException:
                try:
                    r = requests.get("http://" + url, verify=False, timeout=5)
                except requests.exceptions.RequestException:
                    print("Could not reach {}.".format(url))
                    sys.exit(1)
        if r is not None:
            scan_source_code(None, r.text, r.url)


def display_warnings(page_contents):
    src_lines = page_contents.split("\n")
    for line_num in range(len(src_lines)):
        for fun in ["eval(", "document.write(", "document.writeln(", ".innerHTML", ".outerHTML"]:
            line = src_lines[line_num]
            if line.find(fun) != -1:
                print("Dangerous function in line:", line_num)
                print(line)
                print("Check user input")
                print("-" * 50)


def scan_all_files_in_dir(root_dir_path):
    global args
    if os.path.isdir(root_dir_path):
        for dirpath, dirnames, filenames in os.walk(root_dir_path):
            for file in filenames:
                if file.endswith(".html") or file.endswith(".js"):
                    scan_source_code(os.path.join(dirpath, file))
    else:
        print("Error: Invalid path to directory provided.")
        sys.exit(1)


parse_args()
