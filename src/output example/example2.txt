./vuln_detect.py -f ./testWebsite.html 
--------------------------------------------------
Scan results for file: ./testWebsite.html

 [+]  library detected: jquery
 [+]  version: 3.5.1
 [+]  library detected: angularjs
 [+]  version: 0.4.0
=====================================================
 [+] Vulnerable library detected: angularjs
 [+] Vulnerable version: 0.4.0
 [+] summary: XSS may be triggered in AngularJS applications that sanitize user-controlled HTML snippets before passing them to JQLite methods like JQLite.prepend, JQLite.after, JQLite.append, JQLite.replaceWith, JQLite.append, new JQLite and angular.element.
 [+] CVE: ['CVE-2020-7676']
 [+] severity: medium
 [+] info: ['https://github.com/advisories/GHSA-5cp4-xmrw-59wf']

=====================================================
 [+] Vulnerable library detected: angularjs
 [+] Vulnerable version: 0.4.0
 [+] summary: angular.js prior to 1.8.0 allows cross site scripting. The regex-based input HTML replacement may turn sanitized code into unsanitized one.
 [+] CVE: ['CVE-2020-7676']
 [+] severity: low
 [+] info: ['https://nvd.nist.gov/vuln/detail/CVE-2020-7676']

=====================================================
 [+] Vulnerable library detected: angularjs
 [+] Vulnerable version: 0.4.0
 [+] summary: Prototype pollution
 [+] severity: medium
 [+] info: ['https://github.com/angular/angular.js/commit/726f49dcf6c23106ddaf5cfd5e2e592841db743a', 'https://github.com/angular/angular.js/blob/master/CHANGELOG.md#179-pollution-eradication-2019-11-19']

=====================================================
 [+] Vulnerable library detected: angularjs
 [+] Vulnerable version: 0.4.0
 [+] summary: Universal CSP bypass via add-on in Firefox
 [+] severity: medium
 [+] info: ['https://github.com/mozilla/addons-linter/issues/1000#issuecomment-282083435', 'http://pastebin.com/raw/kGrdaypP']

=====================================================
 [+] Vulnerable library detected: angularjs
 [+] Vulnerable version: 0.4.0
 [+] summary: DOS in $sanitize
 [+] severity: medium
 [+] info: ['https://github.com/angular/angular.js/blob/master/CHANGELOG.md', 'https://github.com/angular/angular.js/pull/15699']

=====================================================
 [+] Vulnerable library detected: angularjs
 [+] Vulnerable version: 0.4.0
 [+] summary: XSS in $sanitize in Safari/Firefox
 [+] severity: low
 [+] info: ['https://github.com/angular/angular.js/commit/8f31f1ff43b673a24f84422d5c13d6312b2c4d94']

 [+]  library detected: angularjs
 [+]  version: 1.6.9
=====================================================
 [+] Vulnerable library detected: angularjs
 [+] Vulnerable version: 1.6.9
 [+] summary: XSS may be triggered in AngularJS applications that sanitize user-controlled HTML snippets before passing them to JQLite methods like JQLite.prepend, JQLite.after, JQLite.append, JQLite.replaceWith, JQLite.append, new JQLite and angular.element.
 [+] CVE: ['CVE-2020-7676']
 [+] severity: medium
 [+] info: ['https://github.com/advisories/GHSA-5cp4-xmrw-59wf']

=====================================================
 [+] Vulnerable library detected: angularjs
 [+] Vulnerable version: 1.6.9
 [+] summary: angular.js prior to 1.8.0 allows cross site scripting. The regex-based input HTML replacement may turn sanitized code into unsanitized one.
 [+] CVE: ['CVE-2020-7676']
 [+] severity: low
 [+] info: ['https://nvd.nist.gov/vuln/detail/CVE-2020-7676']

=====================================================
 [+] Vulnerable library detected: angularjs
 [+] Vulnerable version: 1.6.9
 [+] summary: Prototype pollution
 [+] severity: medium
 [+] info: ['https://github.com/angular/angular.js/commit/726f49dcf6c23106ddaf5cfd5e2e592841db743a', 'https://github.com/angular/angular.js/blob/master/CHANGELOG.md#179-pollution-eradication-2019-11-19']

